package src;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        // String which the FSM will read
        String text = "ABBAABABBAABA";
        // List of all nodes
        ArrayList<Node> nodes = new ArrayList<Node>();
        // List to keep track of the order of the node states
        ArrayList<Integer> stateOrder = new ArrayList<Integer>();

        // Make 4 nodes and add them to the node list
        Node node1 = new Node(0);
        nodes.add(node1);
        Node node2 = new Node(1);
        nodes.add(node2);
        Node node3 = new Node(2);
        nodes.add(node3);
        Node node4 = new Node(3);
        nodes.add(node4);

        // Add transitions for all nodes
        for (Node n: nodes) {
            n.setTransitions(nodes);
        }

        // Set the current node to node1 because we always start at state 0
        Node currentNode = node1;

        for(char c: text.toCharArray()) {

            // All available characters
            String chars = "AB";
            // Add the current state to the list
            stateOrder.add(currentNode.getState());

            // If the character is 'A'
            if(c == chars.charAt(0)) {
                // Set the new current node
                currentNode = currentNode.getaTransitionNode();
            }
            // If the character is 'B'
            else if (c == chars.charAt(1)) {
                currentNode = currentNode.getbTransitionNode();
            }
        }

        System.out.println(stateOrder);
    }
}
