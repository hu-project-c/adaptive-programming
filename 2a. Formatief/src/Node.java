package src;

import java.util.ArrayList;
import java.util.Random;

public class Node {

    int state;
    Node aTransitionNode, bTransitionNode;

    public Node(int state) {
        this.state = state;
    }

    public void setTransitions(ArrayList<Node> nodes) {
        // Make copy of the nodes list
        ArrayList<Node> nodesCopy = new ArrayList<>(nodes);

        // Take a random node from the nodes list and set it as A's transitions node
        Node transitionNode = nodes.get(new Random().nextInt(nodesCopy.size()));
        aTransitionNode = transitionNode;
        // Remove the node so we don't get a duplicate transition
        nodesCopy.remove(transitionNode);

        // Take a random node for B's transition node
        bTransitionNode = nodes.get(new Random().nextInt(nodesCopy.size()));
    }

    public Node getaTransitionNode() {
        return aTransitionNode;
    }

    public Node getbTransitionNode() {
        return bTransitionNode;
    }

    public int getState() {
        return state;
    }
}
