import javafx.util.Pair;
import java.util.ArrayList;

public class Node {

    // Store the state of the Node
    String state;
    // Store all the transitions for a node
    private ArrayList<Pair> transitions = new ArrayList<>();

    public Node(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public Pair getTransistion(char c) throws NullPointerException {
        for(Pair pair: this.transitions) {
            if (pair.getKey().toString().charAt(0) == c) {
                return pair;
            }
        }

        throw new NullPointerException("No transition found");
    }

    public void addTransition(char c, Node node) {
        this.transitions.add(new Pair<>(c, node));
    }
}
