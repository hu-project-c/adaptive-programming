import javafx.util.Pair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Machine {

    // Array with all the available characters for the FSM
    private List<Character> availableChars =  new ArrayList<>();
    // List where all the nodes will be stored
    private ArrayList<Node> nodes = new ArrayList<>();
    // List to keep track of which states the FSM has passed
    private ArrayList<String> states = new ArrayList<>();
    // How many nodes the FSM needs to have
    private final int nodeCount;

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public Machine(String availableChars, int nodeCount) {

        for(char c: availableChars.toCharArray()) {
            this.availableChars.add(c);
        }

        this.nodeCount = nodeCount;

        this.generateNodes();
        this.generateNodeTransitions(this.nodes);
    }

    protected void generateNodes() {
        for (int i = 0; i < this.nodeCount; i++) {
            Node node = new Node("s" + i);
            this.nodes.add(node);
        }
    }

    private void generateNodeTransitions(ArrayList<Node> nodes) {
        for(Node node : nodes) {

            // Shuffle available chars so the transitions will be more random
            List<Character> charsCopy = new ArrayList<>(this.availableChars);
            Collections.shuffle(charsCopy);

            // Make copy of the nodes list
            ArrayList<Node> nodesCopy = new ArrayList<>(nodes);

            for(int i = 0; i < this.nodeCount; i++) {

                Node transitionNode = nodes.get(new Random().nextInt(nodesCopy.size()));
                char character = charsCopy.get(i);

                node.addTransition(character, transitionNode);
            }
        }
    }

    public List<Character> generateRandomInput() {
        List<Character> input = new ArrayList<>();

        // Create a random string with all the available characters which we will give to the FSM
        for (int i = 0; i < 10; i++) {
            input.add(this.availableChars.get(new Random().nextInt(this.availableChars.size())));
        }

        return input;
    }

    public ArrayList<String> go() throws NullPointerException {
        // List to keep track of which states the FSM has passed
        ArrayList<String> states = new ArrayList<>();

        List<Character> input = generateRandomInput();
        System.out.println(input);

        Node currentNode = this.nodes.get(0);

        states.add(currentNode.getState());

        for (char c: input) {
            Pair transition = currentNode.getTransistion(c);
            currentNode = (Node) transition.getValue();
            states.add(currentNode.getState());
        }

        return states;
    }


}
