import javafx.util.Pair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {

    @Test
    void getTransistion() {
        ArrayList<Pair> transitions = new ArrayList<>();

        Node node0 = new Node("s0");
        Node node1 = new Node("s1");

        char c = 'B';

        transitions.add(new Pair<>(c, node1));
        node0.addTransition(c, node1);

        assertEquals(node1, node0.getTransistion(c).getValue());
    }
}