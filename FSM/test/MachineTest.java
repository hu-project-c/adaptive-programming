import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MachineTest {

    @Test
    void generateRandomInput() {
        Machine fsm = new Machine("ABCDE", 5);
        List<Character> input = fsm.generateRandomInput();
        assertEquals(10, input.size());
    }

    @Test
    void generateNodes() {
        Machine fsm = new Machine("ABC", 3);
        assertEquals(fsm.getNodes().size(), 3);
    }
}