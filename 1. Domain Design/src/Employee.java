public interface Employee {

    double calculateHourlyWage();
}
