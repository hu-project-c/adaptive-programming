import java.util.Objects;

public class AirCrewMember extends Person implements Employee {

    private String position;

    public AirCrewMember(String name, String gender, int age, String position) {
        super(name, gender, age);
        this.position = position;
    }

    @Override
    public double calculateHourlyWage() {
        return age * 0.6;
    }

    @Override
    public String toString() {
        return "AirCrewMember : " +
                "position='" + position + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AirCrewMember that = (AirCrewMember) o;
        return Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), position);
    }
}
