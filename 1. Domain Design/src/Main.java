import java.util.ArrayList;

class Main {

    public static void main(String[] args) {
        Airport airport = new Airport("Amsterdam", 6);
        System.out.println(airport);

        Airplane airplane = new Airplane("Boeing", "747", 300);
        System.out.println(airplane);

        ArrayList<Passenger> passengers = new ArrayList();
        Passenger passenger = new Passenger("Thijmen", "Male", 19, "Business");
        passengers.add(passenger);
        Passenger passenger2 = new Passenger("Thijmen", "Male", 19, "Economy");
        passengers.add(passenger2);
        Passenger passenger3 = new Passenger("Thijmen", "Male", 19, "Business");
        passengers.add(passenger3);

        ArrayList<AirCrewMember> airCrewMembers = new ArrayList();
        AirCrewMember airCrewMember = new AirCrewMember("Lorem", "Male", 37, "Front");
        airCrewMembers.add(airCrewMember);
        AirCrewMember airCrewMember2 = new AirCrewMember("Dolorem", "Female", 26, "Middle");
        airCrewMembers.add(airCrewMember2);
        AirCrewMember airCrewMember3 = new AirCrewMember("Amet", "Male", 41, "Back");
        airCrewMembers.add(airCrewMember3);

        ArrayList<Pilot> pilots = new ArrayList();
        Pilot pilot = new Pilot("Ipsum", "Female", 45, false);
        pilots.add(pilot);
        Pilot coPilot = new Pilot("Dolar", "Male", 43, true);
        pilots.add(coPilot);

        Flight flight = new Flight("Oslo", "10 minuten");
        flight.setPassengers(passengers);
        flight.setAirCrewMembers(airCrewMembers);
        flight.setPilots(pilots);

        System.out.println(flight);

    }
}
