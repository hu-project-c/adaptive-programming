import java.util.Objects;

public class Pilot extends Person implements Employee {

    private boolean isCoPilot;

    public Pilot(String name, String gender, int age, boolean isCoPilot) {
        super(name, gender, age);
        this.isCoPilot = isCoPilot;
    }

    public double calculateHourlyWage() {
        return age * 0.9;
    }

    @Override
    public String toString() {
        return "Pilot : " +
                "isCoPilot=" + isCoPilot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pilot pilot = (Pilot) o;
        return isCoPilot == pilot.isCoPilot;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isCoPilot);
    }
}
