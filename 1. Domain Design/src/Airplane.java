import java.util.Objects;

public class Airplane {

    private String manufacturer, type;
    private int numberOfSeats;

    public Airplane(String manufacturer, String type, int numberOfSeats) {
        this.manufacturer = manufacturer;
        this.type = type;
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Airplane : " +
                "manufacturer='" + manufacturer + '\'' +
                ", type='" + type + '\'' +
                ", numberOfSeats=" + numberOfSeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airplane airplane = (Airplane) o;
        return numberOfSeats == airplane.numberOfSeats && Objects.equals(manufacturer, airplane.manufacturer) && Objects.equals(type, airplane.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturer, type, numberOfSeats);
    }
}
