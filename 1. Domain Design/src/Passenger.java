import java.util.Objects;

public class Passenger extends Person {

    private String flightClass;

    public Passenger(String name, String gender, int age, String flightClass) {
        super(name, gender, age);
        this.flightClass = flightClass;
    }

    @Override
    public String toString() {
        return "Passenger : " +
                "flightClass='" + flightClass + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Passenger passenger = (Passenger) o;
        return Objects.equals(flightClass, passenger.flightClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), flightClass);
    }
}
