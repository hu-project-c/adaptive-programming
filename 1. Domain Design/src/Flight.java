import java.util.ArrayList;
import java.util.Objects;

public class Flight {

    private String destination, ETA;

    ArrayList<AirCrewMember> airCrewMembers;
    ArrayList<Pilot> pilots;
    ArrayList<Passenger> passengers;

    public Flight(String destination, String ETA) {
        this.destination = destination;
        this.ETA = ETA;
    }

    public String getDestination() {
        return destination;
    }

    public String getETA() {
        return ETA;
    }

    public void setAirCrewMembers(ArrayList<AirCrewMember> airCrewMembers) {
        this.airCrewMembers = airCrewMembers;
    }

    public void setPilots(ArrayList<Pilot> pilots) {
        this.pilots = pilots;
    }

    public void setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return "Flight : " +
                "destination='" + destination + '\'' +
                ", ETA='" + ETA + '\'' +
                ", airCrewMembers=" + airCrewMembers +
                ", pilots=" + pilots +
                ", passengers=" + passengers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(destination, flight.destination) && Objects.equals(ETA, flight.ETA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, ETA);
    }
}
