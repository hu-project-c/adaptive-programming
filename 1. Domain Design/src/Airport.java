import java.util.Objects;

public class Airport {

    private String location;
    private int numberOfRunways;

    public Airport(String location, int numberOfRunways) {
        this.location = location;
        this.numberOfRunways = numberOfRunways;
    }

    @Override
    public String toString() {
        return "Airport : " +
                "location='" + location + '\'' +
                ", numberOfRunways=" + numberOfRunways;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airport airport = (Airport) o;
        return numberOfRunways == airport.numberOfRunways && Objects.equals(location, airport.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, numberOfRunways);
    }
}
